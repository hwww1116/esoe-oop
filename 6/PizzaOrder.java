/**
 * allows up to three pizzas to be saved in an order.
 */
public class PizzaOrder {

    private int numberPizzas = 1;
    private Pizza pizza1;
    private Pizza pizza2;
    private Pizza pizza3;

    /**
     * sets the number of pizzas in the order. numberPizzas and return false.
     * 
     * @param numberPizzas must be between 1 and 3.
     * @return If numberPizzas is not in the range, the order is not valid. Return
     *         true for valid order.
     */
    public boolean setNumberPizzas(int numberPizzas) {
        if (1 <= numberPizzas && numberPizzas <= 3) {
            this.numberPizzas = numberPizzas;
            return true;
        }
        return false;
    }


    /**
     * sets the third pizza in the order.
     * 
     * @param pizza3 the pizza3 to set
     */
    public void setPizza3(Pizza pizza3) {
        this.pizza3 = pizza3;
    }


    /**
     * sets the second pizza in the order.
     * 
     * @param pizza2 the pizza2 to set
     */
    public void setPizza2(Pizza pizza2) {
        this.pizza2 = pizza2;
    }


    /**
     * sets the first pizza in the order.
     * 
     * @param pizza1 the pizza1 to set
     */
    public void setPizza1(Pizza pizza1) {
        this.pizza1 = pizza1;
    }

    
    /**
     * calculates the total cost of the order
     * 
     * @return double
     */
    public double calcTotal() { 
        double total = 0;

        if (this.numberPizzas >= 1) {
            total += this.pizza1.calcCost();
        } 
        if (this.numberPizzas >= 2) {
            total += this.pizza2.calcCost();
        } 
        if (this.numberPizzas >= 3) {
            total += this.pizza3.calcCost();
        }

        return total; 
    }
    
   
}