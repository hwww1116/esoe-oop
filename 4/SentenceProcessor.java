import java.util.*;
import java.util.stream.*;

public class SentenceProcessor {
    public static int staticValue = 0;
    public int value = 123;
    
    
    /** 
     * @param str
     * @return String
     */
    public String removeDuplicatedWords(String str) {
        str = Arrays.stream(str.split("\\s+")).distinct().collect(Collectors.joining(" "));
        return str;
    }

    
    /** 
     * @param word
     * @param replacement
     * @param str
     * @return String
     */
    public String replaceWord(String word, String replacement, String str) {
        // handle word boundary
        String regex = String.format("\\b%s\\b", word);
        return str.replaceAll(regex, replacement);
    }
}