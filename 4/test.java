public class test {

    
    /** 
     * @param args
     */
    public static void main(final String[] args) {
        final SentenceProcessor sp1 = new SentenceProcessor();
        final SentenceProcessor sp2 = new SentenceProcessor();

        System.out.println(sp1.staticValue);
        System.out.println(sp2.staticValue);

        sp1.staticValue = 999;

        System.out.println(sp1.staticValue);
        System.out.println(sp2.staticValue);

        System.out.println(sp2.value);
        System.out.println(sp2.value);
    }
}
