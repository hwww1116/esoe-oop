import java.util.Arrays;

/**
 * When saving objects, array is a good choice, but sometimes it is inconvenient
 * due to its fixed size. Therefore, you have to implement a new data structure
 * – a resizable-array called SimpleArrayList class to save data. The class uses
 * an Integer array as its internal instance variable to save integer, and its
 * size needs to be changed automatically based on user's manipulation. As
 * elements are added to a SimpleArrayList, its size grows automatically. On the
 * contrary, its size decreases while elements are removed from the array. To
 * make this class useful, implement it with the following methods:
 * 
 * @author hwchang
 */

public class SimpleArrayList {

    private Integer[] list;
    private int size = 0;

    /**
     * Default constructor that sets the array size to zero.
     */
    public SimpleArrayList() {
        this.size = 0;
    }

    /**
     * Constructor that can set initial array size, and set all Integer to zero in
     * the array.
     * 
     * @param initialSize is an non-negative integer.
     */
    public SimpleArrayList(int initialSize) {
        this.size = initialSize;
        this.list = new Integer[initialSize];
        for (int i = 0; i < this.size; i++) {
            this.list[i] = 0;
        }
    }

    /**
     * appends the specified element to the end of this array.
     * 
     * @param element
     */
    public void add(Integer element) {
        this.size += 1;
        Integer[] newList = new Integer[this.size];
        for (int i = 0; i < this.size - 1; i++ ) {
            newList[i] = this.list[i];
        }
        newList[this.size - 1] = element;
        this.list = newList;
    }

    /**
     * returns the element at the specified position in this array. If the specified
     * position is out of range of the array, returns null.
     * 
     * @param index
     * @return Integer
     */
    public Integer get(int index) {
        if (index < 0 || this.size <= index) {
            return null;
        }

        return this.list[index];
    }

    /**
     * replaces the element at the specified position in this array with the
     * specified element, and returns the original element at that specified
     * position. If the specified position is out of range of the array, returns
     * null.
     * 
     * @param index
     * @param element
     * @return originalElement
     */
    public Integer set(int index, Integer element) {
        if ( index < 0 || this.size <= index ) {
            return null;
        }

        Integer originalElement = this.list[index];
        this.list[index] = element;
        return originalElement;
    }

    /**
     * If a null element is at the specified position, returns false; otherwise
     * removes it and returns true. Shifts any subsequent elements to the left if
     * removes succesfully.
     * 
     * @param i
     * @return boolean
     */
    public boolean remove(int index) {
        if (this.list[index] == null) {
            return false;
        }

        int newSize = this.size - 1;
        Integer[] newList = new Integer[newSize];
        for (int i = 0; i < index; i++) {
            newList[i] = this.list[i];
        }
        for (int i = index; i < newSize; i++) {
            newList[i] = this.list[i+1];
        }
        this.list = newList;
        this.size = newSize;
        return true;
    }

    /**
     * removes all of the elements from this array.
     */
    public void clear() {
        this.list = null;
        this.size = 0;

    }

    /**
     * returns the number of elements in this array.
     * 
     * @return int
     */
    public int size() {
        return this.size;
    }

    /**
     * retains only the elements in this array that are contained in the specified
     * SimpleArrayList . In other words, removes from this array all of its elements
     * that are not contained in the specified SimpleArrayList. Returns true if this
     * array changed as a result. See examples below:
     * 
     * @param list2
     * @return boolean true if this array changed as a result
     */
    public boolean retainAll(SimpleArrayList simpleArrayList2) {
        boolean isChanged = false;

        for (int i = 0; i < this.size; i++) {
            int find = Arrays.binarySearch(simpleArrayList2.list, this.list[i]);
            if (find < 0) {
                this.remove(i);
                isChanged = true;
            }
        }

        return isChanged;
    }

    

}