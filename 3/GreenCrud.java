public class GreenCrud {
    
    /** 
     * @param initialSize
     * @param days
     * @return int
     */
    public static int calPopulation(int initialSize, int days) {
        
        // initialize
        int growTimes = days / 5;
        //  0, initialSize
        int size1 = 0;
        int size2 = initialSize;

        for (int i = 0; i < growTimes; i++) {
            // fn+2 = fn + fn+1
            int size3 = size1 + size2;
            size1 = size2;
            size2 = size3;
        }

        return size2;
    }
}