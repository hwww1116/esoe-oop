
/**
 * For extensibility and maintenance, you should create a factory class named
 * ShapeFactory to create all kinds of Shape for main class. It contains an enum
 * named Type that store all kinds of shape. It also contains a method named
 * createShape to create instance of all shapes refer to Type parameter.
 */
public class ShapeFactory {

    /**
     * con;tains 3 kinds of shapes as values.
     */
    public enum Type {
        Triangle, Circle, Square
    }
    

    /**
     * returns an object which 'is-a' Shape object.
     * 
     * @param shapeType
     * @param length
     * @return an object which 'is-a' Shape object.
     */
    public Shape createShape(ShapeFactory.Type shapeType, double length) {
        this.Type = shapeType;
        switch(shapeType) {
            case Circle:
                return new Circle(length);
            case Square:
                return new Square(length);
            case Triangle:
               return new Triangle(length);
            default:
                return null; 
            
        }
    }
}