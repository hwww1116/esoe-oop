/**
 * please define Triangle (treat it as regular triangle), Circle and Square
 * classes that extend Shape class and implement all abstract methods. For
 * Circle class, length means its diameter.
 * 
 * @author hwchang
 */

public class Square extends Shape{

    public Square(double length) {
        super(length);
    }

    @Override
    public void setLength(double length) {
        this.length = length;
    }

    /**
     * @return Area of Circle
     */
    @Override
    public double getArea() {
        double area = Math.pow(this.length, 2);
        return Math.round(area * 100.0) / 100.0;
    }

    @Override
    public double getPerimeter() {
        double perimeter = 4 * this.length;
        return Math.round(perimeter * 100.0) / 100.0;
    }

}