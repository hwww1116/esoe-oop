/**
 *  an abstract class named Shape with some methods and variables that can
 * get information of this geometric shape
 * 
 * @author unknown
 */

public abstract class Shape {

    /**
     * stores the side length for regular triangle, square, and stores the diameter
     * for circle.
     */
    protected double length;

    public Shape(double length) {
        this.length = length;
    }

    /**
     * set the length ofx this shape.
     * 
     * @param length
     */
    public abstract void setLength(double length);

    public abstract double getArea();

    public abstract double getPerimeter();


    /**
     * print the area and perimeter of this shape. The output format is as below:
     * "Area = 47.74, Perimeter = 31.5" If the number is zero, the output should be
     * "0.0". 
     * 
     * @return
     */
    public String getInfo() {
        return "Area = " + getArea() + ", Perimeter = " + getPerimeter();
    }
}