
/**
 * Circle 
 * length means its diameter.
 * 
 * @author hwchang
 */

public class Circle extends Shape{

    public Circle(double length) {
        super(length);
    }

    @Override
    public void setLength(double length) {
        this.length = length;
    }

    /**
     * @return Area of Circle
     */
    @Override
    public double getArea() {
        double r = this.length / 2; 
        double area = Math.PI * Math.pow(r, 2);
        return Math.round(area * 100.0) / 100.0;
    }

    @Override
    public double getPerimeter() {
        double r = this.length / 2;
        double perimeter = 2 * Math.PI * r;
        return Math.round(perimeter * 100.0) / 100.0;
    }
    

}