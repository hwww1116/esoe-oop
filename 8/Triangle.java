/**
 * please define Triangle (treat it as regular triangle), 
 * Circle and Square classes that extend Shape class and implement all abstract methods. 
 * For Circle class, length means its diameter.
 * 
 * @author hwchang
 */
public class Triangle extends Shape {

    public Triangle(double length) {
        super(length);
    }

    @Override
    public void setLength(double length) {
       this.length = length;
    }

    /**
     * @return Area of Triangle
     */
    @Override
    public double getArea() {
        double area = Math.sqrt(3) / 4 * Math.pow(this.length, 2);
        return  Math.round(area * 100.0) / 100.0;
    }

    @Override
    public double getPerimeter() {
        double perimeter = 3 * this.length;
        return Math.round(perimeter * 100.0) / 100.0;
    }

}