package two;

import one.B;

public class D extends B {
    public void demo() {
        doStuff();
    }
    public void demo2() {
        D object = new D();
        object.doStuff();
    }
    
    public void demo3() {
        B object = new B();
        // object.doStuff();
    }

}