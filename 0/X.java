import java.util.List;

import javax.sound.midi.SysexMessage;

class Outer {
    static int i;
    final static int j = 0;

    class Inner {
        static int ii = 0;
        final static int jj = 0;
    }

    static void print1(List<String> lst) {
        for (String element : lst) {
            System.out.print(element + " ");
        }
    }

    static void print2(List<String> lst) {
        for (Object element : lst) {
            System.out.print(element + " ");
        }
    }

    static void print3(List<?> lst) {
        for (Object element : lst) {
            System.out.print(element + " ");
        }
    }

    static <T> void print4(List<T> lst) {
        for (Object element : lst) {
            System.out.print(element + " ");
        }
    }

    static <T> void print5(List<T> lst) {
        for (T element : lst) {
            System.out.print(element + " ");
        }
    }

}

class C {

    public long sum(long a, long b) {
        return a + b;
    }

    public int sum(int a, int b) {
        return a + b;
    }

    public long sum(long a, int b) {
        return a + b;
    }

    private static void w(int v) {
        System.out.println("f");
    }

    public static void main(String[] args) {
        try {
            f();
        } catch (InterruptedException e) {
            System.out.println(1);
            throw new RuntimeException();
        } catch (RuntimeException e) {
            System.out.println(2);
            return;
        } finally {
            System.out.println(4);
        }
        System.out.println(5);
    }

    static void f() throws InterruptedException {
        throw new InterruptedException("df");
    }
}

interface I {
    void setValue(int val);

    int getValue();
}



class E implements I {
    int value;

    public void setValue(int val) {
        value = val;
    }

    @Override
    public int getValue() {
        return 0;
    }
}

class N {

}

public class X {
    protected A m1(A z) throws E1, E3 {
        return new A();
    }
}

class Y extends X{

    public A m1(A z) throws E1 {
        return new B();
    }
}

class A {
    
}

class B extends A {
    
}

class E1 extends Exception{
    
}

class E2 extends Exception{
    
}
class E3 extends Exception {

}