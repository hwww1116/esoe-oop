/**
 * It contains an enumerated type ExceptionTYPE which includes two kinds of
 * exception. To record the detail of exception raised, we need a private
 * variable excptionCondition with the type of ExceptionTYPE we just defined and
 * this variable would be set by constructor. For ATM to get the imformation of
 * exception raised, use getMessage.
 */
public class ATM_Exception extends Exception {

    private static final long serialVersionUID = 1L;

    public enum ExceptionTYPE {
        BALANCE_NOT_ENOUGH, AMOUNT_INVALID;
    }

    private ExceptionTYPE excptionCondition;

    public ATM_Exception(String errMessage) {
        super(errMessage);
    }

    public ATM_Exception(ExceptionTYPE excptionCondition) {
        this.excptionCondition = excptionCondition;
    }

    @Override
    public String getMessage() {
        switch (excptionCondition) {
            case BALANCE_NOT_ENOUGH:
                return "BALANCE_NOT_ENOUGH";
            case AMOUNT_INVALID:
                return "AMOUNT_INVALID";
            default:
                return null;

        }
    }
}
