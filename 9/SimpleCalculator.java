import java.text.DecimalFormat;
import java.util.regex.Pattern;

/**
 * Write a program that can serve as a simple calculator. This calculator keeps
 * track of a single number (of type double) that is called result and that
 * starts out as 0.00. Each cycle allows the user to repeatedly add, subtract,
 * multiply, or divide by a second number. The result of one of these operations
 * becomes the new value of result. The calculation ends when the user enters
 * the letter R for “result” (either in upper- or lowercase).
 * 
 * @author hwchang
 */

public class SimpleCalculator {

    /**
     * maintains the calculation result which is not rounded yet.
     */
    private double result = 0.00;

    public int count = 0;
    private double value = 0;
    private char operator = 'f';
    private boolean finished = false;
    DecimalFormat formatter = new DecimalFormat("0.00");

    /**
     * parse the command and set new result. If command is not valid, throws an
     * UnknownCmdException contained the error message.
     * 
     * @param cmd is the command in the format mentioned above
     * @throws UnknownCmdException
     */
    public void calResult(String cmd) throws UnknownCmdException {

        // Exception
        if (cmd.length() < 3 || cmd.charAt(1) != ' ') {
            throw new UnknownCmdException(String.format("Please enter 1 operator and 1 value separated by 1 space"));
        }
        String[] cmd_arr = cmd.split(" ");
        if (cmd_arr.length > 2) {
            throw new UnknownCmdException(String.format("Please enter 1 operator and 1 value separated by 1 space"));
        }

        operator = cmd.charAt(0);

        String valueString = cmd.substring(2);

        // Exception
        if (!isNumeric(valueString) && operator != '+' && operator != '-' && operator != '*' && operator != '/') {
            throw new UnknownCmdException(
                    String.format("%c is an unknown operator and %s is an unknown value", operator, valueString));
        } else if (!isNumeric(valueString)) {
            throw new UnknownCmdException(String.format("%s is an unknown value", valueString));
        } else if (operator != '+' && operator != '-' && operator != '*' && operator != '/') {
            throw new UnknownCmdException(String.format("%c is an unknown operator", operator));
        }

        value = Double.parseDouble(valueString);

        if (value == 0) {
            throw new UnknownCmdException(String.format("Can not divide by 0"));
        }

        if (operator == '+') {
            result += value;
        }
        if (operator == '-') {
            result -= value;
        }
        if (operator == '*') {
            result *= value;
        }
        if (operator == '/') {
            result /= value;
        }

        count += 1;

    }

    /**
     * output the message of the result of this step. The output message depends on
     * the calculation count. The output format is as below: (1)when starting the
     * calculation, it will return "Calculator is on. Result = [result]" (2)when
     * calculation count = 1, it will return "Result [operator] [value] = [result].
     * New result = [result]" (3)when calculation count > 1, it will return "Result
     * [operator] [value] = [result]. Updated result = [result]" (4)when the
     * calculation finish, it will return "Final result = [result]"
     * 
     * @return the result of the command, or throws an exception with an error
     *         message in it.
     */
    public String getMsg() {

        String roundedValue = formatter.format(value);
        String roundedResult = formatter.format(result);

        if (finished) {
            return String.format("Final result = %s", roundedResult);
        }
        if (count == 0) {
            return String.format("Calculator is on. Result = %s", roundedResult);
        } else if (count == 1) {
            return String.format("Result %c %s = %s. New result = %s", operator, roundedValue, roundedResult,
                    roundedResult);
        } else if (count > 1) {
            return String.format("Result %c %s = %s. Updated result = %s", operator, roundedValue, roundedResult,
                    roundedResult);
        }
        return " ";
    }

    /**
     * determine whether calculation comes to the end or not.
     * 
     * @param cmd
     * @return true if cmd is "r" or "R", otherwise false.
     */
    public boolean endCalc(String cmd) {
        if (cmd.equals("r") || cmd.equals("R")) {
            finished = true;
        }
        return finished;
    }

    private Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");

    public boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        return pattern.matcher(strNum).matches();
    }
}