
public class test {

    /**
     * @param args
     */
    public static void main(String[] args) {
        String[] ans = { "Calculator is on. Result = 0.00", "Result + 5.00 = 5.00. New result = 5.00",
                "Result - 2.00 = 3.00. Updated result = 3.00", "Result * 5.00 = 15.00. Updated result = 15.00",
                "Result / 3.00 = 5.00. Updated result = 5.00", "% is an unknown operator", "D is an unknown value",
                "X is an unknown operator and D is an unknown value",
                "Please enter 1 operator and 1 value separated by 1 space",
                "Please enter 1 operator and 1 value separated by 1 space",
                "Please enter 1 operator and 1 value separated by 1 space",
                "Result / 1000000.00 = 0.00. Updated result = 0.00", "Can not divide by 0",
                "Result / 0.00 = 5.00. Updated result = 5.00",
                "Please enter 1 operator and 1 value separated by 1 space",
                "Result - 1.67 = 3.33. Updated result = 3.33", "r is an unknown operator and R is an unknown value",
                "Final result = 3.33" };

        SimpleCalculator cal = new SimpleCalculator();
        String cmd = null;
        System.out.println(cal.getMsg());
        String cmd_str = "+ 5,- 2,* 5,/ 3,% 2,* D,X D,XD,, ,/ 1000000,/ 00.000,/ 0.000001,+ 1 + 1,- 1.66633,r R,r";
        String[] cmd_arr = cmd_str.split(",");
        for (int i = 0; i < cmd_arr.length; i++) {
            try {
                if (cal.endCalc(cmd_arr[i]))
                    break;
                cal.calResult(cmd_arr[i]);
                String msg = cal.getMsg();
                System.out.println(msg);
                System.out.println(msg.equals(ans[i + 1]));
            } catch (UnknownCmdException e) {
                System.out.println(e.getMessage());
                System.out.println(e.getMessage().equals(ans[i + 1]));
            }
        }
        System.out.println(cal.getMsg());
    }

}