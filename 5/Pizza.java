public class Pizza {
    private String size;
    private int numberOfCheese;
    private int numberOfPepperoni;
    private int numberOfHam;

    public Pizza() {
        this.size = "small";
        this.numberOfCheese = 1;
        this.numberOfPepperoni = 1;
        this.numberOfHam = 1;
    }

    public Pizza(final String newSize, final int newNumberOfCheese, final int newNumberOfPepperoni,
            final int newNumberOfHam) {
        this.size = newSize;
        this.numberOfCheese = newNumberOfCheese;
        this.numberOfPepperoni = newNumberOfPepperoni;
        this.numberOfHam = newNumberOfHam;
    }

    
    /** 
     * @param newSize
     */
    public void setSize(final String newSize) {
        this.size = newSize;
    }

    
    /** 
     * @param newNumberOfCheese
     */
    public void setNumberOfCheese(final int newNumberOfCheese) {
        this.numberOfCheese = newNumberOfCheese;
    }

    
    /** 
     * @param newNumberOfPepperoni
     */
    public void setNumberOfPepperoni(final int newNumberOfPepperoni) {
        this.numberOfPepperoni = newNumberOfPepperoni;
    }

    
    /** 
     * @param newNumberOfHam
     */
    public void setNumberOfHam(final int newNumberOfHam) {
        this.numberOfHam = newNumberOfHam;
    }

    
    /** 
     * @return String
     */
    public String getSize() {
        return this.size;
    }

    
    /** 
     * @return int
     */
    public int getNumberOfCheese() {
        return this.numberOfCheese;
    }

    
    /** 
     * @return int
     */
    public int getNumberOfPepperoni() {
        return this.numberOfPepperoni;
    }

    
    /** 
     * @return int
     */
    public int getNumberOfHam() {
        return this.numberOfHam;
    }

    /**
     * return a double that is the cost of the pizza. Pizza cost is determined by:
     * <ul>
     * <li>Small: $10 + $2 per topping
     * <li>Medium: $12 + $2 per topping
     * <li>Large: $14 + $2 per
     * </ul>
     * topping For example, a large pizza with one cheese, one pepperoni and two ham
     * toppings should cost a total of $22.
     * 
     * @return <code>double</code> the cost of the pizza
     */
    public double calcCost() {
        double cost = 0;
        int numberOfTopping = this.numberOfCheese + this.numberOfHam + this.numberOfPepperoni;
        if (this.size == "small") {
            cost = 10 + 2 * numberOfTopping;
        } else if (this.size == "medium") {
            cost = 12 + 2 * numberOfTopping;
        } else if (this.size == "large") {
            cost = 14 + 2 * numberOfTopping;
        }
        return cost;
    }

    /**
     * An overload method equals() that determine whether this pizza is the same as
     * the other one by their size, the number of cheese toppings, the number of
     * pepperoni toppings, and the number of ham toppings.
     * 
     * @param anotherPizza
     * @return boolean
     */
    public boolean equals(final Pizza anotherPizza) {
        if (this.size == anotherPizza.size //
                && this.numberOfCheese == anotherPizza.numberOfCheese //
                && this.numberOfHam == anotherPizza.numberOfHam //
                && this.numberOfPepperoni == anotherPizza.numberOfPepperoni) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * An override method toString() that return a String containing the pizza size,
     * quantity of each topping. The output format is as below: 
     * <blockqoute>
     * <pre>
     * size = small, numOfCheese = 0, numOfPepperoni = 0, numOfHam = 0
     * </pre></blockqoute>
     * @return String string representation of pizza containing the pizza size, quantity of each topping.
     */
    public String toString() {
        String pizzaDescription;
        pizzaDescription = "size = " + this.size //
                + ", numOfCheese = " + this.numberOfCheese //
                + ", numOfPepperoni = " + this.numberOfPepperoni //
                + ", numOfHam = " + this.numberOfHam;
        return pizzaDescription;
    }
}